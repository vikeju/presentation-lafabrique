# Tronc commun - Projet 1 : présentation en Markdown

But : réaliser une présentation en utilisant des outils de développeurs.

## Introduction

Vous n'en avez pas marre des présentations PowerPoint pourries ? Vous aussi vous pensez que [ça rend stupide](https://www.editionsladecouverte.fr/catalogue/index-La_pensee_PowerPoint-9782707159533.html) ?

Vous allez pouvoir enfin dire au revoir à ce logiciel aliénant en développant vous-même votre présentation !

Pourquoi faire cela quand on peut utiliser la souris me direz-vous ?

Tout d'abord parce que c'est cool :).

Ensuite, parce que coder sa présentation permet de la gérer avec un outil de gestion de version de sources (GVS) comme git et ainsi pouvoir garder un historique clair des modifications.

Enfin, parce que HTML et Markdown vous permettent des formatages vraiment puissants, notamment la coloration syntaxique que PowerPoint ne sait pas faire. Puis, votre présentation étant en HTML, elle peut très facilement être mise en ligne.

## Reveal.js

[Reveal.js](https://revealjs.com) est un outil merveilleux qui permet de créer rapidement de chouettes présentations utilisant HTML/CSS/JS et même [Markdown](https://www.markdownguide.org/cheat-sheet), outil de formatage de texte simplissime et très lisible, utilisé massivement dans le monde du développement logiciel.


## Consignes

Vous allez devoir réaliser une présentation web en vous basant sur l'outil reveal.js et la publier sur GitLab à l'aide des GitLab Pages.



### Fork

Si vous ne l'avez pas encore déjà fait, forkez ce projet dans Gitlab afin d'en avoir une _copie_ sur votre espace personnel.

Une fois le projet forké dans Gitlab, vous allez devoir récupérer une copie sur votre ordinateur. Cela se fait avec la commande `git clone`.

Ouvrez Git bash si vous êtes sous Windows ou le Terminal sous Mac ou Linux et tapez par exemple :


```bash
# Si vous ne l'avez pas déjà fait, configurer git avec votre nom et votre email
# Remplacez <Prénom Nom> Par votre Prénom et votre nom
git config --global user.name "<Prénom Nom>"
# Remplacez <mon@email.com> par votre adresse email
git config --global user.email "<mon@email.com>"


# Remplacez <user> par votre nom d'utilisateur GitLab
git clone git@framagit.org:<user>/presentation-lafabrique.git

# Si ça ne fonctionne pas utiliser l'url suivante
git clone https://framagit.org/<user>/presentation-lafabrique.git
```

Vous avez maintenant un dossier `presentation-lafabrique` qui contient les fichiers nécessaires à la présentation.

### Première modif

Allez dans le dossier `presentation-lafabrique`.

Ouvrer le fichier `index.html` avec votre navigateur, cay bô non ?

Maintenant éditez le fichier `index.html` avec un éditeur de texte comme `notepad++` si vous êtes sous Windows.

Rajoutez votre nom et prénom quelque part dans un des slides.

Rouvrez le fichier dans le navigateur pour vous assurer que votre nom s'affiche bien.

### Premier commit

Maintenant, il va falloir intégrer vos modifications dans l'historique git et les publier sur votre projet gitlab.

Vous pouvez faire un `git status` pour voir quels sont les fichiers qui ont été modifiés (ici seulement `index.html`).

__Note : Pour plus d'info sur les commandes git vous pouvez lire [cette micro](https://framagit.org/Andreas/cheatsheet-lafabrique/blob/master/cheatsheet_git.md) doc que j'ai rédigé.__

Ensuite il faut ajouter le fichier `index.html` au commit que vous voulez créer :

`git add index.html`

Puis faire le commit en précisant un message par exemple :

`git commit -m "Ajout du nom"`

Et enfin, publier vos modifications sur le serveur Gitlab (appelé `origin` par convention) :

`git push origin master`


__Note : Si vous vous êtes trompé de mot de passe lors de l'authentification et que maintenant il ne vous demande plus votre mot de passe mais affiche une erreur d'authentification, vous devez réinitialiser git en tappant la commande suivante :__

`git config --system --unset credential.helper`

__Si vous n'avez pas le droit de lancer cette commande, relancez Git Bash en mode Administrateur (bouton droit "Executer en tant qu'administrateur").__

Accédez à la page gitlab de votre projet et assurez vous que votre modification s'y trouve bien.

## Réalisation du contenu

Maintenant que vous savez modifier votre présentation, sauvegarder vos modifications dans git et les envoyer sur Gitlab, vous pouvez rajouter des slides à votre présentation en suivant les recommandations présentent dans les consignes papiers.

Inspirez-vous du fichier `demo.html` pour voir tout ce qui est possible de faire en matière de transition et de positionnement des pages.

Le contenu des slides doit **impérativement** être écrit en [Markdown](https://www.markdownguide.org/cheat-sheet).

Commitez votre travail (plusieurs fois même si vous le souhaitez) et publiez le sur Gitlab.

## Les Gitlab pages

Gitlab offre la possibilité d'héberger directement du contenu html statique via ce qu'ils appellent les [Gitlab Pages](https://docs.gitlab.com/ce/user/project/pages/index.html). C'est un outil super pratique qui évite d'avoir à prendre un hébergement ailleurs pour le site internet de votre projet.

Normalement, tout est bien configuré pour que votre projet soit automatiquement déployé à l'url suivante :

`https://<user>.frama.io/presentation-lafabrique/`

Remplacez `<user>` par votre nom d'utilisateur Gitlab et ça devrait aller !

Vous arrivez à accéder au site web ? Super, vous avez déployé votre premier site web !

Vous pouvez maintenant faire votre présentation depuis n'importe quel terminal web où que vous soyez dans le monde, elle est pas belle la vie ?